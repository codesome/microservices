package com.codesome.billing.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(name = "activeMq")
public class ActiveMQSettings {
	private String host;
	private String billingQueue;
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getBillingQueue() {
		return billingQueue;
	}
	public void setBillingQueue(String billingQueue) {
		this.billingQueue = billingQueue;
	}
	
}
