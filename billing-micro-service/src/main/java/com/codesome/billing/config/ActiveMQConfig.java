package com.codesome.billing.config;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.connection.CachingConnectionFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.SimpleMessageListenerContainer;
import org.springframework.jms.listener.adapter.MessageListenerAdapter;

import com.codesome.billing.listener.BillingListener;

@Configuration
@EnableConfigurationProperties(ActiveMQSettings.class)
public class ActiveMQConfig {

	@Autowired
	private ActiveMQSettings activeMQSettings;
	
	@Bean
    public ConnectionFactory connectionFactory() {
        return new CachingConnectionFactory(
                new ActiveMQConnectionFactory(activeMQSettings.getHost()));
    }

    @Bean
    public MessageListenerAdapter receiver() {
        return new MessageListenerAdapter(new BillingListener()) {{
            setDefaultListenerMethod("onMessage");
        }};
    }

    @Bean
	public SimpleMessageListenerContainer container(
			final MessageListenerAdapter messageListener,
			final ConnectionFactory connectionFactory) {
		return new SimpleMessageListenerContainer() {
			{
				setMessageListener(messageListener);
				setConnectionFactory(connectionFactory);
				setDestinationName(activeMQSettings.getBillingQueue());
			}
		};
	}

    @Bean
    public JmsTemplate jmsTemplate(ConnectionFactory connectionFactory) {
        return new JmsTemplate(connectionFactory);
    }
}
