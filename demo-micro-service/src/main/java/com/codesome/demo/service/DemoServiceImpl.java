package com.codesome.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class DemoServiceImpl implements DemoService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DemoServiceImpl.class);

	@Override
	public String helloWorld(String userName) {
		// TODO Auto-generated method stub
		return "Hello World from " + userName;
	}
	
}