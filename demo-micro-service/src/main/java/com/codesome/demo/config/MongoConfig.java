package com.codesome.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

/*@Configuration
@EnableConfigurationProperties(MongoSettings.class)*/
public class MongoConfig {
	
	@Autowired
	private MongoSettings mongoSettings;

    @Bean
    public MongoDbFactory mongoDbFactory() throws Exception {
        UserCredentials userCredentials = null;

        String mongoDBUser = mongoSettings.getUserName();
        if (mongoDBUser != null && !mongoDBUser.isEmpty()) {
            userCredentials = new UserCredentials(mongoDBUser, mongoSettings.getPassword());
        }

        return new SimpleMongoDbFactory(new MongoClient(new MongoClientURI(
        		mongoSettings.getUri())),
        		mongoSettings.getDatabaseName(),
                userCredentials);
    }

    @Bean(name = "demoDBOperations")
    public MongoOperations mongoOperations() throws Exception {
        MongoOperations operations = new MongoTemplate(mongoDbFactory());
        String collectionName = mongoSettings.getCollectionName();
        if (!operations.collectionExists(collectionName)) {
            operations.createCollection(collectionName);
        }
        return operations;
    }

}

