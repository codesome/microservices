package com.codesome.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/*@ConfigurationProperties(name = "mongo")*/
public class MongoSettings {

	private String uri;
	private String userName;
	private String password;
	private String databaseName;
	private String collectionName;
	
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getCollectionName() {
		return collectionName;
	}
	public void setCollectionName(String collectionName) {
		this.collectionName = collectionName;
	}
}

