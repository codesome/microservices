package com.codesome.demo.controller;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.codesome.demo.config.ActiveMQSettings;
import com.codesome.demo.service.DemoService;

@RestController
@RequestMapping("/api/user/")
public class DemoController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DemoController.class);

	@Autowired
	private DemoService demoService;
	
	@Autowired
	private ActiveMQSettings activeMQSettings;
	
	@Autowired
	private JmsTemplate jmsTemplate;

    @RequestMapping(value = "helloworld/{userName}", method = RequestMethod.GET)
    public @ResponseBody String send(@PathVariable String userName, final HttpServletRequest request) throws Exception {
    	String response = demoService.helloWorld(userName);
    	publishBillingMessage(userName);
    	return response;
    }
    
	public void publishBillingMessage(final String message) {
		jmsTemplate.send(activeMQSettings.getBillingQueue(), new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createTextMessage(message);
			}
		});
	}

    
    @ResponseStatus(org.springframework.http.HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ Exception.class })
	public @ResponseBody String handleValidationException(Exception ex) {
    	ex.printStackTrace();
		return "{\"code\":\"400\",  \"systemMessage\":" + ex.getMessage() + "}";
	}

}

