#! /bin/bash -e

rm -fr build
mkdir build
cp ../target/demo-micro-service-1.0.0-SNAPSHOT.jar build

docker build -t demo-micro-service .
